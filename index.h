
// Written by: Tracey DeCorte
// CST420 - Assignment 2
// Indexer
// index.h




#ifndef index_H
#define index_H


#include "entry.h"

#include <fstream>
#include <string>
#include <set>

using namespace std;




class Index
{
	typedef multiset<Entry> Entries;
	typedef Entries::iterator Iter;

	Entries m_entries;
	string m_skipName;

public:

	Index(string docName, string skipName);

	void display();
	void processLine(string line, int pageNum, int lineNum);
	bool findWord( string wordToFind );
	bool findDuplicate( string word, int pageNum, int lineNum );

};




#endif