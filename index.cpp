
// Written by: Tracey DeCorte
// CST420 - Assignment 2
// Indexer
// index.cpp



#include "index.h"


#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <locale>
#include <sstream>
#include <string>





using namespace std;





// Converts to lowercase and removes non-alphabetic chars from string

string convertLower( string word )
{
	locale loc;
	string lowerWord;


	for (size_t i = 0; i < word.length(); ++i)
	{
		if (word[i] >= 'A' && word[i] <= 'Z' || word[i] >= 'a' && word[i] <= 'z' 
					|| word[i] ==' ')
			lowerWord += tolower(word[i], loc);

	}

	return lowerWord;
}




// Compares word passed in to words in file

bool Index::findWord( string wordToFind )
{
	string word;

	std::ifstream skipFile( m_skipName );

		
	if ( !skipFile )
	{
		cout << "File open failed.";
		exit( EXIT_FAILURE );
	}


	while ( !skipFile.eof() )
	{
		skipFile >> word;

		if ( word == wordToFind )
		{
			skipFile.close();
			return true;
		}
	}

	skipFile.close();
	return false;
}




// For the line from the file passed in, processes string
// and adds each word to Entry object

void Index::processLine( string line, int pageNum, int lineNum )
{

	bool found = false;
	bool duplicate = false;

	istringstream in( line );


	while ( ! in.eof() )
	{
		found = false;
		duplicate = false;

		string token;

		in >> token;

		token = convertLower( token );

		found = findWord( token );

		if ( found == true )
			continue;


		Entry entry( token, pageNum, lineNum );
		
		m_entries.insert( entry );
	}

}




// Reads in lines from doc file and processes one line at a time

Index::Index( string docName, string skipName )
{
	m_skipName = skipName;

	std::ifstream docFile( docName );



	if ( !docFile )
	{
		cout << "File open failed.";
		exit( EXIT_FAILURE );
	}


	string line;	
	int pageNum = 1;
	int lineNum = 0;



	while ( getline( docFile, line ) )
	{
		lineNum++;

		if ( line == "<newpage>" )
		{
			pageNum++;
			lineNum = 0;
			continue;
		}


		if ( line.length() == 0 )
			continue;

		processLine( line, pageNum, lineNum );
	}


	docFile.close(); 

}




// Displays entries as organized output, removing duplicate entries
// and duplicate occurrences from being displayed

void Index::display()
{	

	cout << endl << "Index" << endl;
	cout << "-----"; 
	


	string tempWord;
	int tempPage = 0;
	int tempLine = 0;


	for ( Iter iter = m_entries.begin(); iter != m_entries.end(); ++iter)
	{

		Entry entry = *iter;


		if ( tempWord != entry.getWord() )
		{
			cout << endl << entry.getWord() << endl << setw(3);
			cout << *iter;
		}
		
		else 
		{	
			if ( entry.getPageNum() == tempPage && entry.getLineNum() == tempLine )
				continue;

			cout << ", " << *iter;
		}
		
		tempWord = entry.getWord();
		tempPage = entry.getPageNum();
		tempLine = entry.getLineNum();
		
	}

	cout << endl;
}


