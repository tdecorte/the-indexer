
// Written by: Tracey DeCorte
// CST420 - Assignment 2
// Indexer
// entry.h




#ifndef entry_H
#define entry_H


#include <set>
#include <string>


using namespace std;







class Entry
{

	string m_word;
	int m_pageNum;
	int m_lineNum;	
	

public:

	Entry() {}
	Entry( string word, int pageNum, int lineNum );

	string getWord() const { return m_word; }
	int getPageNum() const { return m_pageNum; }
	int getLineNum() const { return m_lineNum; }

};





ostream &operator<< ( ostream &lhs, const Entry &rhs );

bool operator< ( const Entry &lhs, const Entry &rhs );






#endif