
// Written by: Tracey DeCorte
// CST420 - Assignment 2
// July 18, 2013
// Indexer
// entry.cpp




#include "entry.h"
#include "index.h"






// Member assignment on construction

Entry::Entry( string word, int pageNum, int lineNum )
{
	m_word = word;
	m_pageNum = pageNum;
	m_lineNum = lineNum;
}




// Overloads less than operator to compare by word, page and line

bool operator< ( const Entry &lhs, const Entry &rhs )
{
	if (lhs.getWord() < rhs.getWord())
		return true;

	if (lhs.getWord() > rhs.getWord())
		return false;

	if (lhs.getPageNum() < rhs.getPageNum())
		return true;

	if (lhs.getPageNum() > rhs.getPageNum())
		return false;

	if (lhs.getLineNum() < rhs.getLineNum())
		return true;
	
	return false;
}




// Overloads ostream operator to display page and line in format: 0.0

ostream &operator<< ( ostream &lhs, const Entry &rhs )
{
	
	return lhs << rhs.getPageNum() << '.' << rhs.getLineNum();
		
}