
// Written by: Tracey DeCorte
// CST420 - Assignment 2
// Indexer
// main.cpp



#include <iostream>

#include "Entry.h"
#include "Index.h"



using namespace std;




int main()
{

	string docFile;
	string skipFile;


	cout << "Enter the document file name: ";
	cin >> docFile;

	cout << "Enter the skip-words file name: ";
	cin >> skipFile;


	Index index(docFile, skipFile);

	index.display();



	return 0;
}